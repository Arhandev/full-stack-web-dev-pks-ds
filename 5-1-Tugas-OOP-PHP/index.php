<?php
trait Hewan{
    public $nama;
    public $jumlahKaki;
    public $darah;
    public $keahlian;

    public function atraksi(){
        echo $this->nama . " " . $this->keahlian . "<br>";
    }
}

trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang($musuh){
        echo $this->nama . " sedang menyerang " . $musuh->nama. "<br>";
        $musuh->diserang($this);
    }

    public function diserang($musuh){
        echo $this->nama . " sedang diserang " . "<br>" ;
        $this->darah = $this->darah-$musuh->attackPower/$this->defencePower;
    }
}

class Elang{
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower,  $darah = 50)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        $this->darah = $darah;
    }

    public function getInfoHewan(){
        echo $this->nama . "<br>";
        echo $this->jumlahKaki . "<br>";
        echo $this->keahlian . "<br>";
        echo $this->darah . "<br>";
        echo $this->attackPower . "<br>";
        echo $this->defencePower . "<br>";
    }
}

class Harimau{
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower, $darah = 50)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        $this->darah = $darah;
    }

    public function getInfoHewan(){
        echo $this->nama . "<br>";
        echo $this->jumlahKaki . "<br>";
        echo $this->keahlian . "<br>";
        echo $this->darah . "<br>";
        echo $this->attackPower . "<br>";
        echo $this->defencePower . "<br>";
    }
}


$elang = new Elang("Elang_1", 2, "terbang tinggi", 10, 5);
$harimau = new Harimau("Harimau_1", 4, "lari cepat", 7, 8);

$elang->serang($harimau);
$harimau->serang($elang);

$elang->getInfoHewan();
$harimau->getInfoHewan();