// Soal 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];
daftarHewan.sort();
console.log(daftarHewan);

// Soal 2
const introduce = dict => {
	console.log(
		`Nama saya ${dict.name}, umur saya ${dict.age} tahun, alamat saya di ${dict.address}, dan saya punya hobby yaitu ${dict.hobby}`
	);
};

const dictionary = {
	name: 'Aan',
	age: 20,
	address: 'Jalan Teratai Putih',
	hobby: 'Coding',
};

introduce(dictionary);

// Soal 3
const hitung_huruf_vokal = kata => {
	let counter = 0;
	kata = kata.split('');
	let result = kata.filter(kata_single => {
		return (
			kata_single == 'a' ||
			kata_single == 'i' ||
			kata_single == 'u' ||
			kata_single == 'e' ||
			kata_single == 'o' ||
			kata_single == 'y' ||
			kata_single == 'A' ||
			kata_single == 'I' ||
			kata_single == 'U' ||
			kata_single == 'E' ||
			kata_single == 'O' ||
			kata_single == 'Y'
		);
	});

	return result.length;
};

var hitung_1 = hitung_huruf_vokal('Muhammad');

var hitung_2 = hitung_huruf_vokal('Iqbal');

console.log(hitung_1, hitung_2); // 3 2

// Soal 4

const hitung = integer => {
	return 2 * integer - 2;
};

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8