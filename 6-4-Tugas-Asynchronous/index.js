//soal 1
var readBooks = require('./callback.js');
const readBooksPromise = require('./promise.js');

var books = [
	{ name: 'LOTR', timeSpent: 3000 },
	{ name: 'Fidas', timeSpent: 2000 },
	{ name: 'Kalkulus', timeSpent: 4000 },
	{ name: 'komik', timeSpent: 1000 },
];

//jawaban soal 1
let index = 0;

const callback = sisaWaktu => {
	index += 1;
	if (sisaWaktu == 0) {
		console.log('Habis waktu');
		return () => {};
	}
	readBooks(sisaWaktu, books[index], callback);
};

readBooks(10000, books[index], callback);

// soal 2

var books = [
	{ name: 'LOTR', timeSpent: 3000 },
	{ name: 'Fidas', timeSpent: 2000 },
	{ name: 'Kalkulus', timeSpent: 4000 },
	{ name: 'komik', timeSpent: 1000 },
];

//jawaban soal 2
let indexPromise = 0;

const callbackPromise = sisaWaktu => {
	indexPromise += 1;
	if (sisaWaktu == 0) {
		console.log('Habis waktu');
		return () => {};
	}
	readBooksPromise(sisaWaktu, books[indexPromise])
		.then(callbackPromise)
		.catch(error => console.log(error));
};

// Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[indexPromise])
	.then(callbackPromise)
	.catch(error => console.log(error));
