<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Comment extends Model
{
    protected $table = "comments";
    protected $primaryKey = "id";
    protected $keyTypes = "string";
    protected $fillable = ["content", "post_id", "user_id"];
    public $incrementing = false;
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }
    
    public function post(){
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
