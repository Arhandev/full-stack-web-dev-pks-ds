<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password'   => 'required|confirmed|min:6',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = User::where('email', $request->email)->first();

        if(!$user){
            return response()->json([
                "success"=>false,
                "messafe"=>"User tidak ditemukan",
            ], 400);
        }


        $user->update([
            "password"=>Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password Berhasil diubah',
            'data' => [
                "user" => $user,
            ],
        ], 200);

    }
}
