<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    protected $table = "posts";
    protected $primaryKey = "id";
    protected $keyTypes = "string";
    protected $fillable = ["title", "description", "user_id"];
    public $incrementing = false;
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->user_id = auth()->user()->id;
        });
    }

    public function comments(){
        return $this->hasMany('App\Comment', 'post_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
