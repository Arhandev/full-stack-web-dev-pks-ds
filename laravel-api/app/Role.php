<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    protected $table = "roles";
    protected $primaryKey = "id";
    protected $keyTypes = "string";
    protected $fillable = ['id',"name"];
    public $incrementing = false;
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
