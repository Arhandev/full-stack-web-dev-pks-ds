<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('roles', 'RoleController');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::apiResource('comments', 'CommentController');
    Route::apiResource('posts', 'PostController');
});

 
 Route::group([
     'prefix'=>"auth",
     'namespace'=>"Auth"
 ], function(){
      Route::post('register', 'RegisterController')->name('auth.register');
      Route::post('login', 'LoginController')->name('auth.login');
      Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerateotp_code');
      Route::post('verification', 'VerificationController')->name('auth.verification');
      Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
});
